﻿
namespace Winforms_15._2._5
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaam = new System.Windows.Forms.Label();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.btnNieuw = new System.Windows.Forms.Button();
            this.btnLiefkozen = new System.Windows.Forms.Button();
            this.btnEten = new System.Windows.Forms.Button();
            this.btnStraffen = new System.Windows.Forms.Button();
            this.btnGevoel = new System.Windows.Forms.Button();
            this.lblEten = new System.Windows.Forms.Label();
            this.txtEten = new System.Windows.Forms.TextBox();
            this.lblStraffen = new System.Windows.Forms.Label();
            this.txtStraffen = new System.Windows.Forms.TextBox();
            this.txtGevoel = new System.Windows.Forms.TextBox();
            this.lblKeuze = new System.Windows.Forms.Label();
            this.cmbTamagotchi = new System.Windows.Forms.ComboBox();
            this.btnLaden = new System.Windows.Forms.Button();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNaam
            // 
            this.lblNaam.AutoSize = true;
            this.lblNaam.Location = new System.Drawing.Point(126, 31);
            this.lblNaam.Name = "lblNaam";
            this.lblNaam.Size = new System.Drawing.Size(39, 15);
            this.lblNaam.TabIndex = 0;
            this.lblNaam.Text = "Naam";
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(205, 27);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(100, 23);
            this.txtNaam.TabIndex = 1;
            // 
            // btnNieuw
            // 
            this.btnNieuw.Location = new System.Drawing.Point(33, 27);
            this.btnNieuw.Name = "btnNieuw";
            this.btnNieuw.Size = new System.Drawing.Size(75, 23);
            this.btnNieuw.TabIndex = 2;
            this.btnNieuw.Text = "Nieuw";
            this.btnNieuw.UseVisualStyleBackColor = true;
            this.btnNieuw.Click += new System.EventHandler(this.btnNieuw_Click);
            // 
            // btnLiefkozen
            // 
            this.btnLiefkozen.Location = new System.Drawing.Point(33, 150);
            this.btnLiefkozen.Name = "btnLiefkozen";
            this.btnLiefkozen.Size = new System.Drawing.Size(75, 23);
            this.btnLiefkozen.TabIndex = 3;
            this.btnLiefkozen.Text = "Liefkozen";
            this.btnLiefkozen.UseVisualStyleBackColor = true;
            this.btnLiefkozen.Click += new System.EventHandler(this.btnLiefkozen_Click);
            // 
            // btnEten
            // 
            this.btnEten.Location = new System.Drawing.Point(33, 194);
            this.btnEten.Name = "btnEten";
            this.btnEten.Size = new System.Drawing.Size(75, 23);
            this.btnEten.TabIndex = 4;
            this.btnEten.Text = "Eten";
            this.btnEten.UseVisualStyleBackColor = true;
            this.btnEten.Click += new System.EventHandler(this.btnEten_Click);
            // 
            // btnStraffen
            // 
            this.btnStraffen.Location = new System.Drawing.Point(33, 243);
            this.btnStraffen.Name = "btnStraffen";
            this.btnStraffen.Size = new System.Drawing.Size(75, 23);
            this.btnStraffen.TabIndex = 5;
            this.btnStraffen.Text = "Straffen";
            this.btnStraffen.UseVisualStyleBackColor = true;
            this.btnStraffen.Click += new System.EventHandler(this.btnStraffen_Click);
            // 
            // btnGevoel
            // 
            this.btnGevoel.Location = new System.Drawing.Point(33, 296);
            this.btnGevoel.Name = "btnGevoel";
            this.btnGevoel.Size = new System.Drawing.Size(75, 50);
            this.btnGevoel.TabIndex = 6;
            this.btnGevoel.Text = "Hoe gaat het?";
            this.btnGevoel.UseVisualStyleBackColor = true;
            this.btnGevoel.Click += new System.EventHandler(this.btnGevoel_Click);
            // 
            // lblEten
            // 
            this.lblEten.AutoSize = true;
            this.lblEten.Location = new System.Drawing.Point(126, 198);
            this.lblEten.Name = "lblEten";
            this.lblEten.Size = new System.Drawing.Size(73, 15);
            this.lblEten.TabIndex = 7;
            this.lblEten.Text = "Hoeveelheid";
            // 
            // txtEten
            // 
            this.txtEten.Location = new System.Drawing.Point(205, 194);
            this.txtEten.Name = "txtEten";
            this.txtEten.Size = new System.Drawing.Size(100, 23);
            this.txtEten.TabIndex = 8;
            // 
            // lblStraffen
            // 
            this.lblStraffen.AutoSize = true;
            this.lblStraffen.Location = new System.Drawing.Point(126, 247);
            this.lblStraffen.Name = "lblStraffen";
            this.lblStraffen.Size = new System.Drawing.Size(73, 15);
            this.lblStraffen.TabIndex = 9;
            this.lblStraffen.Text = "Hoeveelheid";
            // 
            // txtStraffen
            // 
            this.txtStraffen.Location = new System.Drawing.Point(205, 244);
            this.txtStraffen.Name = "txtStraffen";
            this.txtStraffen.Size = new System.Drawing.Size(100, 23);
            this.txtStraffen.TabIndex = 10;
            // 
            // txtGevoel
            // 
            this.txtGevoel.Location = new System.Drawing.Point(126, 296);
            this.txtGevoel.Multiline = true;
            this.txtGevoel.Name = "txtGevoel";
            this.txtGevoel.ReadOnly = true;
            this.txtGevoel.Size = new System.Drawing.Size(179, 194);
            this.txtGevoel.TabIndex = 11;
            // 
            // lblKeuze
            // 
            this.lblKeuze.AutoSize = true;
            this.lblKeuze.Location = new System.Drawing.Point(126, 69);
            this.lblKeuze.Name = "lblKeuze";
            this.lblKeuze.Size = new System.Drawing.Size(115, 15);
            this.lblKeuze.TabIndex = 12;
            this.lblKeuze.Text = "Kies een Tamagotchi";
            // 
            // cmbTamagotchi
            // 
            this.cmbTamagotchi.FormattingEnabled = true;
            this.cmbTamagotchi.Location = new System.Drawing.Point(126, 102);
            this.cmbTamagotchi.Name = "cmbTamagotchi";
            this.cmbTamagotchi.Size = new System.Drawing.Size(179, 23);
            this.cmbTamagotchi.TabIndex = 13;
            // 
            // btnLaden
            // 
            this.btnLaden.Location = new System.Drawing.Point(33, 102);
            this.btnLaden.Name = "btnLaden";
            this.btnLaden.Size = new System.Drawing.Size(75, 23);
            this.btnLaden.TabIndex = 14;
            this.btnLaden.Text = "Laden";
            this.btnLaden.UseVisualStyleBackColor = true;
            this.btnLaden.Click += new System.EventHandler(this.btnLaden_Click);
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(33, 65);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(75, 23);
            this.btnOpslaan.TabIndex = 15;
            this.btnOpslaan.Text = "Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 525);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.btnLaden);
            this.Controls.Add(this.cmbTamagotchi);
            this.Controls.Add(this.lblKeuze);
            this.Controls.Add(this.txtGevoel);
            this.Controls.Add(this.txtStraffen);
            this.Controls.Add(this.lblStraffen);
            this.Controls.Add(this.txtEten);
            this.Controls.Add(this.lblEten);
            this.Controls.Add(this.btnGevoel);
            this.Controls.Add(this.btnStraffen);
            this.Controls.Add(this.btnEten);
            this.Controls.Add(this.btnLiefkozen);
            this.Controls.Add(this.btnNieuw);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.lblNaam);
            this.Name = "Form1";
            this.Text = "Tamagotchi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaam;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.Button btnNieuw;
        private System.Windows.Forms.Button btnLiefkozen;
        private System.Windows.Forms.Button btnEten;
        private System.Windows.Forms.Button btnStraffen;
        private System.Windows.Forms.Button btnGevoel;
        private System.Windows.Forms.Label lblEten;
        private System.Windows.Forms.TextBox txtEten;
        private System.Windows.Forms.Label lblStraffen;
        private System.Windows.Forms.TextBox txtStraffen;
        private System.Windows.Forms.TextBox txtGevoel;
        private System.Windows.Forms.Label lblKeuze;
        private System.Windows.Forms.ComboBox cmbTamagotchi;
        private System.Windows.Forms.Button btnLaden;
        private System.Windows.Forms.Button btnOpslaan;
    }
}

