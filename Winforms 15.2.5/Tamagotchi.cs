﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_15._2._5
{
    class Tamagotchi
    {
        private int _goedgevoel;
        private int _honger;
        private DateTime _maaltijd;
        private string _naam;
        private bool _levend;

        public Tamagotchi()
        {
            Goedgevoel = 5;
            Honger = 5;
            Maaltijd = DateTime.Now;
            Naam = "";
            Levend = true;
        }

        public Tamagotchi(string zin)
        {
            string[] array = new string[5];
            array = zin.Split('|');
            Naam = array[0];
            Goedgevoel = Convert.ToInt32(array[1]);
            Honger = Convert.ToInt32(array[2]);
            Maaltijd = Convert.ToDateTime(array[3]);
            Levend = Convert.ToBoolean(array[4]);
        }

        private int Goedgevoel 
        {
            get { return _goedgevoel; }
            set 
            {
                if (value >= -10 && value <= 10)
                {
                    _goedgevoel = value;
                }
            }
        }

        private int Honger
        {
            get { return _honger; }
            set
            {
                if (value >= -10 && value <= 20)
                {
                    _honger = value;
                }
            }
        }

        private DateTime Maaltijd
        {
            get { return _maaltijd; }
            set
            {
                _maaltijd = value;
            }
        }

        public string Naam
        {
            get { return _naam; }
            set
            {
                _naam = value;
            }
        }

        private bool Levend { get { return _levend; } set { _levend = value; } }

        public void Eten(int eetEenheden)
        {
            if (Honger - Math.Floor((DateTime.Now - Maaltijd).TotalSeconds / 30) <= -10)
            {
                Levend = false;
            }
            else if (Levend)
            {
                if (eetEenheden > 3)
                {
                    eetEenheden = 3;
                }
                Honger -= Convert.ToInt32(Math.Floor((DateTime.Now - Maaltijd).TotalSeconds / 30));
                for (int i = 0; i < eetEenheden; i++)
                {
                    Honger++;
                }
                Maaltijd = DateTime.Now;
            }
            
        }

        public string Gevoel()
        {
            string gevoel = Naam;
            double aantalSeconden = (DateTime.Now - Maaltijd).TotalSeconds;
            int counter = 0;
            //Maaltijd = DateTime.Now;

            if (Levend)
            {
                while (aantalSeconden > 30)
                {
                    counter++;
                    aantalSeconden -= 30;
                }
                if (Honger == -10)
                {
                    Levend = false;
                    gevoel += " is dood...";
                }
                else
                {
                    gevoel += " leeft!";
                }
                
            }
            else
            {
                gevoel += " is dood...";
            }

            gevoel += Environment.NewLine + $"Goed gevoel: {Goedgevoel}" + Environment.NewLine + $"Honger: {Honger - counter}." + Environment.NewLine + Maaltijd;

            if (Levend)
            {
                if (Goedgevoel > 0)
                {
                    Goedgevoel--;
                }
            }

            return gevoel;
        }

        public void Liefkozen()
        {
            if (Levend)
            {
                Goedgevoel++;
            }
        }

        public void Straffen(int strafEenheden)
        {
            if (Levend)
            {
                if (Goedgevoel < 0)
                {
                    if (Honger < 0)
                    {
                        strafEenheden *= 3;
                    }
                    else
                    {
                        strafEenheden *= 2;
                    }
                }
                else if (Honger < 0)
                {
                    strafEenheden *= 2;
                }
                for (int i = 0; i < strafEenheden; i++)
                {
                    Goedgevoel--;
                }
                if (Goedgevoel == -10)
                {
                    Levend = false;
                }
            }
        }

        public override string ToString()
        {
            return $"{Naam}|{Goedgevoel}|{Honger}|{Maaltijd}|{Levend}";
        }
    }
}
