﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Winforms_15._2._5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Tamagotchi t1, t2, t3, t4, t5;
        int counter = 0;

        private void btnNieuw_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNaam.Text))
            {
                switch (counter)
                {
                    case 0:
                        btnLiefkozen.Enabled = true;
                        btnEten.Enabled = true;
                        btnStraffen.Enabled = true;
                        btnGevoel.Enabled = true;
                        txtEten.ReadOnly = false;
                        txtStraffen.ReadOnly = false;
                        cmbTamagotchi.Enabled = true;
                        t1 = new Tamagotchi();
                        t1.Naam = txtNaam.Text;
                        break;
                    case 1:
                        t2 = new Tamagotchi();
                        t2.Naam = txtNaam.Text;
                        break;
                    case 2:
                        t3 = new Tamagotchi();
                        t3.Naam = txtNaam.Text;
                        break;
                    case 3:
                        t4 = new Tamagotchi();
                        t4.Naam = txtNaam.Text;
                        break;
                    case 4:
                        t5 = new Tamagotchi();
                        t5.Naam = txtNaam.Text;
                        btnNieuw.Enabled = false;
                        break;
                }
                cmbTamagotchi.Items[counter] = txtNaam.Text;
                cmbTamagotchi.SelectedIndex = counter;
                txtNaam.Text = "";
                counter++;
            }
            else
            {
                MessageBox.Show("Geef naam in!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLiefkozen_Click(object sender, EventArgs e)
        {
            txtGevoel.Text = "";
            if (cmbTamagotchi.SelectedIndex <= counter)
            {
                switch (cmbTamagotchi.SelectedIndex)
                {
                    case 0:
                        t1.Liefkozen();
                        break;
                    case 1:
                        t2.Liefkozen();
                        break;
                    case 2:
                        t3.Liefkozen();
                        break;
                    case 3:
                        t4.Liefkozen();
                        break;
                    case 4:
                        t5.Liefkozen();
                        break;
                }
            }

        }

        private void btnLaden_Click(object sender, EventArgs e)
        {
            string zin;
            if (File.Exists("tamagotchi.txt"))
            {
                using (StreamReader lezen = new StreamReader("tamagotchi.txt"))
                {
                    counter = Convert.ToInt32(lezen.ReadLine());

                    for (int i = 0; i < counter; i++)
                    {
                        zin = lezen.ReadLine();
                        switch (i)
                        {
                            case 0:
                                t1 = new Tamagotchi(zin);
                                cmbTamagotchi.Items[i] = t1.Naam;
                                break;
                            case 1:
                                t2 = new Tamagotchi(zin);
                                cmbTamagotchi.Items[i] = t2.Naam;
                                break;
                            case 2:
                                t3 = new Tamagotchi(zin);
                                cmbTamagotchi.Items[i] = t3.Naam;
                                break;
                            case 3:
                                t4 = new Tamagotchi(zin);
                                cmbTamagotchi.Items[i] = t4.Naam;
                                break;
                            case 4:
                                t5 = new Tamagotchi(zin);
                                cmbTamagotchi.Items[i] = t5.Naam;
                                btnNieuw.Enabled = false;
                                break;
                        }
                    }
                }
                btnLiefkozen.Enabled = true;
                btnEten.Enabled = true;
                btnStraffen.Enabled = true;
                btnGevoel.Enabled = true;
                txtEten.ReadOnly = false;
                txtStraffen.ReadOnly = false;
                cmbTamagotchi.Enabled = true;
            }
            
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            using (StreamWriter opslag = new StreamWriter("tamagotchi.txt"))
            {
                opslag.WriteLine(counter);
                for (int i = 0; i < counter; i++)
                {
                    switch (i)
                    {
                        case 0:
                            opslag.WriteLine(t1.ToString());
                            break;
                        case 1:
                            opslag.WriteLine(t2.ToString());
                            break;
                        case 2:
                            opslag.WriteLine(t3.ToString());
                            break;
                        case 3:
                            opslag.WriteLine(t4.ToString());
                            break;
                        case 4:
                            opslag.WriteLine(t5.ToString());
                            break;
                    }
                }
            }
        }

        private void btnEten_Click(object sender, EventArgs e)
        {
            if (cmbTamagotchi.SelectedIndex <= counter)
            {
                if (!string.IsNullOrEmpty(txtEten.Text) && int.TryParse(txtEten.Text, out int eten))
                {
                    switch (cmbTamagotchi.SelectedIndex)
                    {
                        case 0:
                            t1.Eten(eten);
                            break;
                        case 1:
                            t2.Eten(eten);
                            break;
                        case 2:
                            t3.Eten(eten);
                            break;
                        case 3:
                            t4.Eten(eten);
                            break;
                        case 4:
                            t5.Eten(eten);
                            break;
                    }
                    txtEten.Text = "";
                }
                else
                {
                    MessageBox.Show("Geef een numeriek aantal eten in!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void btnStraffen_Click(object sender, EventArgs e)
        {
            if (cmbTamagotchi.SelectedIndex <= counter)
            {
                if (!string.IsNullOrEmpty(txtStraffen.Text) && int.TryParse(txtStraffen.Text, out int straf))
                {
                    switch (cmbTamagotchi.SelectedIndex)
                    {
                        case 0:
                            t1.Straffen(straf);
                            break;
                        case 1:
                            t2.Straffen(straf);
                            break;
                        case 2:
                            t3.Straffen(straf);
                            break;
                        case 3:
                            t4.Straffen(straf);
                            break;
                        case 4:
                            t5.Straffen(straf);
                            break;
                    }
                    txtStraffen.Text = "";
                }
                else
                {
                    MessageBox.Show("Geef een numerieke straf in!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnGevoel_Click(object sender, EventArgs e)
        {
            if (cmbTamagotchi.SelectedIndex <= counter)
            {
                switch (cmbTamagotchi.SelectedIndex)
                {
                    case 0:
                        txtGevoel.Text = t1.Gevoel();
                        break;
                    case 1:
                        txtGevoel.Text = t2.Gevoel();
                        break;
                    case 2:
                        txtGevoel.Text = t3.Gevoel();
                        break;
                    case 3:
                        txtGevoel.Text = t4.Gevoel();
                        break;
                    case 4:
                        txtGevoel.Text = t5.Gevoel();
                        break;
                }
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnLiefkozen.Enabled = false;
            btnEten.Enabled = false;
            btnStraffen.Enabled = false;
            btnGevoel.Enabled = false;
            txtEten.ReadOnly = true;
            txtStraffen.ReadOnly = true;
            cmbTamagotchi.Enabled = false;

            for (int i = 0; i < 5; i++)
            {
                cmbTamagotchi.Items.Add("Leeg");
            }
        }
    }
}
